# remove any features with empties
# onehot encode left over
# cluster onehot encoded
# fill blank values based on cluster averages
import pandas as pd
import re
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
from sklearn.metrics.pairwise import cosine_similarity
from math import *

filename = 'userprofile.csv'

input_df = pd.read_csv('./modified_data/' + filename)
replace_cols = ['smoker', 'dress_preference', 'ambience', 'transport', 'marital_status', 'hijos', 'activity', 'budget']
new_df = input_df.drop(columns=replace_cols)

# ------------------------------------------------------------------------
# onehot_encode:

# values to not onehot encode: ['latitude', 'longitude', 'weight', 'height', 'birth_year']
not_onehot = ['userID', 'latitude', 'longitude', 'weight', 'height', 'birth_year']
user_cluster = new_df.filter(not_onehot, axis=1)

drop = ['latitude', 'longitude', 'weight', 'height', 'birth_year']

userprofile_df = new_df
userprofile_df = userprofile_df.drop(columns=drop)
userpayment_df = pd.read_csv('./modified_data/userpayment.csv')
usercuisine_df = pd.read_csv('./modified_data/usercuisine.csv')
# replace userID with values without U
ids = userpayment_df['userID']
n_ids = []
for e in ids:
    ne = re.sub('U', '', e)
    n_ids.append(ne)
userpayment_df.drop(columns=['userID'])
userpayment_df['userID'] = n_ids

ids = userprofile_df['userID']
n_ids = []
for e in ids:
    ne = re.sub('U', '', e)
    n_ids.append(ne)
userprofile_df.drop(columns=['userID'])
userprofile_df['userID'] = n_ids

ids = usercuisine_df['userID']
n_ids = []
for e in ids:
    ne = re.sub('U', '', e)
    n_ids.append(ne)
usercuisine_df.drop(columns=['userID'])
usercuisine_df['userID'] = n_ids

# one hot encodes the data
one_hot_cuisine = pd.get_dummies(usercuisine_df['Rcuisine'])
usercuisine_df = usercuisine_df.drop(['Rcuisine'], axis=1)
# joins the one hot encoded data and then aggregates it
usercuisine_df = usercuisine_df.join(one_hot_cuisine).groupby('userID').max()

# one hot encode the payment type
one_hot_payment = pd.get_dummies(userpayment_df['Upayment'])
userpayment_df = userpayment_df.drop(['Upayment'], axis=1)
userpayment_df = userpayment_df.join(one_hot_payment).groupby('userID').max()

# join the dataframes all together into one dataframe
user_df = pd.merge(pd.merge(userprofile_df, userpayment_df, on='userID'), usercuisine_df, on='userID')
columns = user_df.columns

for feature in ['color', 'drink_level', 'interest', 'personality', 'religion']:
    one_hot_userprofile = pd.get_dummies(user_df[feature])
    one_hot_userprofile.rename(columns=lambda x: feature + '_' + str(x), inplace=True)
    user_df = user_df.drop([feature], axis=1)
    user_df = user_df.join(one_hot_userprofile).groupby('userID').max()


n_user_df = user_df.drop(columns=['Unnamed: 0_x', 'Unnamed: 0_y', 'Unnamed: 0'])
print(list(n_user_df.columns))
# index is the id number

# --------------------------------------------------------------------------
# compute the similarity between the users with missing values and other users to fill in missing values
# --------------------------------------------------------------------------
# find users with missing values
input_df = pd.read_csv('./modified_data/' + filename)
missing_cols = ['smoker', 'dress_preference', 'ambience', 'transport', 'marital_status', 'hijos', 'activity', 'budget']

missing_entries = {}
none_counts = []
remove_cols = []

for e in range(0,len(input_df.index)):
    key = input_df.at[e,'userID']
    key = re.sub('U', '', key)
    val = []
    add = False
    for col in input_df:
        if input_df.at[e,col] == '?':
            val.append(1)
            add = True
        else: 
            val.append(0)
    if add == True:
        missing_entries.update({key:val})

print(missing_entries)
print(len(missing_entries))

# for each missing value user, ccompute similarty betwee other users with all values
#   get list for each user info
comparison_df = n_user_df
print(list(missing_entries.keys()))
print(comparison_df)
key_list = list(missing_entries.keys())
key_list.remove('1024')
key_list.remove('1025')
key_list.remove('1122')
key_list.remove('1130')
compare = comparison_df.drop(key_list)

# from: http://dataaspirant.com/2015/04/11/five-most-popular-similarity-measures-implementation-in-python/
def square_rooted(x):
    return round(sqrt(sum([a*a for a in x])),3)
 
def cosine_sim(x,y):
    numerator = sum(a*b for a,b in zip(x,y))
    denominator = square_rooted(x)*square_rooted(y)
    return round(numerator/float(denominator),3)

def euclidean_distance(x,y):
    return sqrt(sum(pow(a-b,2) for a, b in zip(x, y)))

# we want closest to 1
most_sim = {}
for key in key_list: #missing_entries:
    need = comparison_df.loc[[key]].values[0]
    print(need)
    need = [float(i) for i in need]
    max = 0
    sim_vector = []
    for i, row in compare.iterrows():
        r1 = [float(i) for i in row]
        sim = cosine_sim(need,r1)
        key1 = i
        if sim > max:
            max = sim
            sim_vector = key1    
    print(max)
    most_sim.update({key:sim_vector})
    
print(most_sim)

# -----------------------------------------------------------------
# assign missing values based on vector most similar to
moded_df = pd.read_csv('./modified_data/' + filename)
missing_cols = ['smoker', 'dress_preference', 'ambience', 'transport', 'marital_status', 'hijos', 'activity', 'budget']

def find_index(k):
    for i, row in moded_df.iterrows():
        if row['userID'] == 'U'+str(k):
            return i

for key in key_list:
    index_k = find_index(key)
    for col in moded_df:
        if moded_df.at[index_k,col] == '?':
            print(most_sim[key])
            index_s = find_index(most_sim[key])
            moded_df.at[index_k,col] = moded_df.at[index_s, col]

moded_df = moded_df.drop(columns=['Unnamed: 0'])
moded_df.to_csv('./modified_data/' + filename)
