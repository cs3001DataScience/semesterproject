
# coding: utf-8

# In[60]:


import pandas as pd
import numpy as np

userprofile_df = pd.read_csv('./modified_data/userprofile.csv')
userpayment_df = pd.read_csv('./modified_data/userpayment.csv')
usercuisine_df = pd.read_csv('./modified_data/usercuisine.csv')

userprofile_df = userprofile_df.drop(['latitude', 'longitude', 'weight', 'height', 'birth_year', 'marital_status','hijos','interest','personality','religion','activity','color'], axis=1) 

# one hot encodes the data
one_hot_cuisine = pd.get_dummies(usercuisine_df['Rcuisine'])
usercuisine_df = usercuisine_df.drop(['Rcuisine'], axis=1)
# joins the one hot encoded data and then aggregates it
usercuisine_df = usercuisine_df.join(one_hot_cuisine).groupby('userID').max()

# one hot encode the payment type
one_hot_payment = pd.get_dummies(userpayment_df['Upayment'])
userpayment_df = userpayment_df.drop(['Upayment'], axis=1)
userpayment_df = userpayment_df.join(one_hot_payment).groupby('userID').max()

# join the dataframes all together into one dataframe
user_df = pd.merge(pd.merge(userprofile_df, userpayment_df, on='userID'), usercuisine_df, on='userID')

for feature in ['smoker', 'drink_level', 'budget', 'dress_preference', 'ambience', 'transport']:
    one_hot_userprofile = pd.get_dummies(user_df[feature])
    one_hot_userprofile.rename(columns=lambda x: feature + '_' + x, inplace=True)
    user_df = user_df.drop([feature], axis=1)
    user_df = user_df.join(one_hot_userprofile).groupby('userID').max()

user_df = user_df.drop(columns=['Unnamed: 0_x','Unnamed: 0_y','Unnamed: 0'])
user_df.to_csv('./modified_data/users_match.csv')


# In[46]:


chefmozparking_df = pd.read_csv('./modified_data/chefmozparking.csv')
chefmozcusine_df = pd.read_csv('./modified_data/chefmozcuisine.csv')
chefmozhours4_df = pd.read_csv('./modified_data/chefmozhours4.csv')
chefmozaccepts_df = pd.read_csv('./modified_data/chefmozaccepts.csv')
places_df = pd.read_csv('./modified_data/geoplaces2.csv', encoding = "ISO-8859-1")

# dropping:
#   - the_geo_meter because they don't really mean much
#   - name because i dont think that affects the choice, 
places_df = places_df.drop(['latitude','longitude','the_geom_meter', 'name','accessibility','franchise','area','other_services'], axis=1)

# one hot encodes the restaurant cuisine data
one_hot_rcuisine = pd.get_dummies(chefmozcusine_df['Rcuisine'])
restaurantcuisine_df = chefmozcusine_df.drop(['Rcuisine'], axis=1)
restaurantcuisine_df = restaurantcuisine_df.join(one_hot_rcuisine).groupby('placeID').max()

# one hot encode the payment type accepted
one_hot_rpayment = pd.get_dummies(chefmozaccepts_df['Rpayment'])
restaurantpayment_df = chefmozaccepts_df.drop(['Rpayment'], axis=1)
restaurantpayment_df = restaurantpayment_df.join(one_hot_rpayment).groupby('placeID').max()

# one hot encode the parking
one_hot_parking = pd.get_dummies(chefmozparking_df['parking_lot'])
restaurantparking_df = chefmozparking_df.drop('parking_lot', axis = 1)
restaurantparking_df = restaurantparking_df.join(one_hot_parking).groupby('placeID').max()

# join the dataframes all together into one dataframe
geoplaces2_df = pd.merge(places_df, restaurantparking_df, on='placeID')
#geoplaces2_df = geoplaces2_df.drop(columns=['Unnamed: 0_x','Unnamed: 0_y'])
print(geoplaces2_df.columns)
geoplaces2_df = pd.merge(geoplaces2_df, restaurantpayment_df, on='placeID')
geoplaces2_df = geoplaces2_df.drop(columns=['Unnamed: 0_x','Unnamed: 0_y'])
print(geoplaces2_df.columns)
geoplaces2_df = pd.merge(geoplaces2_df, restaurantcuisine_df, on='placeID')
print(geoplaces2_df.columns)
print(geoplaces2_df)

for feature in ['alcohol', 'price', 'dress_code', 'smoking_area', 'Rambience']:
    one_hot_geoplaces = pd.get_dummies(geoplaces2_df[feature])
    geoplaces2_df = geoplaces2_df.drop([feature], axis=1)
    one_hot_geoplaces.rename(columns=lambda x: feature + '_' + x, inplace=True)
    geoplaces2_df = geoplaces2_df.join(one_hot_geoplaces).groupby('placeID').max()

geoplaces2_df.to_csv('./modified_data/places_match.csv')
