import pandas as pd

# create training data file with rating
a = pd.read_csv("./data/train.csv")
b = pd.read_csv("./data/rating_final.csv")
b = b.dropna(axis=1)
merged = a.merge(b, on=['userID','placeID'])
merged = merged.drop(columns=['food_rating','service_rating'])
merged.to_csv("./aggregated_data/train_with_rating.csv", index=False)

# create testing data file with rating
a = pd.read_csv("./data/test.csv")
b = pd.read_csv("./data/rating_final.csv")
b = b.dropna(axis=1)
merged = a.merge(b, on=['userID','placeID'])
merged = merged.drop(columns=['food_rating','service_rating'])
merged.to_csv("./aggregated_data/test_with_rating.csv", index=False)