# remove a feature: if any empty values (only geoplaces2.csv had lots of missing)
    # Later: if more than 50% of entries are NONE, creates new file in modified_data
# if only a few, replace with the average value 
    # future work, to cluster and replace with similar values for that
# only geoplaces2.csv and userprofiles.csv had any missing values

import pandas as pd

filename = 'geoplaces2.csv'

input_df = pd.read_csv('./data/' + filename)
none_counts = []
remove_cols = []
percent_empty = []
for col in input_df:
    #temp = input_df[col]
    n_count = 0
    for e in range(1,len(input_df.index)):
        if input_df.at[e,col] == '?':
            n_count = n_count + 1
    none_counts.append(n_count)
    percent_empty.append(n_count/len(input_df.index))
    if n_count > 0:
        remove_cols.append(col)

print(remove_cols)
print(percent_empty)
print(none_counts)

# remove columns and create new file in modified_data directory
new_df = input_df
new_df = new_df.drop(columns=remove_cols)
new_df.to_csv('./modified_data/' + filename)
        