# ID3 Tree and CART Tree
from sklearn.metrics import accuracy_score, mean_squared_error, mean_absolute_error
from sklearn import tree
import graphviz
import pandas as pd

input_df = pd.read_csv('./aggregated_data/total_train.csv')
Y = input_df['rating']
X = input_df.drop(columns=['rating','asdf','userID','placeID'])

id3 = tree.DecisionTreeClassifier(criterion='entropy')
id3 = id3.fit(X,Y)
cart = tree.DecisionTreeClassifier()
cart = cart.fit(X,Y)

# rating = 2, UID = U1001, PlaceID = 132825
print(id3.predict([[0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,1,0,0,0,0,1,0,0,1,0,0,1]]))

print(cast.predict([[0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,1,0,0,0,0,1,0,0,1,0,0,1]]))

#get testing data
test_df = pd.read_csv('./aggregated_data/total_test.csv')
actual_rate = test_df['rating']
testing = test_df.drop(columns=['rating','asdf','userID','placeID'])

predictions = id3.predict(testing)
print(accuracy_score(actual_rate, predictions)) # 1 is best
print(mean_squared_error(actual_rate, predictions)) # 0 is best
print(mean_absolute_error(actual_rate, predictions)) # 0 is best

predictions = cart.predict(testing)
print(accuracy_score(actual_rate, predictions))
print(mean_squared_error(actual_rate, predictions))
print(mean_absolute_error(actual_rate, predictions))

#features = [asdf,userID,American_Express,MasterCard-Eurocard,VISA,bank_debit_cards,cash,Afghan,African,American,Armenian,Asian,Australian,Austrian,Bagels,Bakery,Bar,Bar_Pub_Brewery,Barbecue,Basque,Brazilian,Breakfast-Brunch,British,Burgers,Burmese,Cafe-Coffee_Shop,Cafeteria,Cajun-Creole,California,Cambodian,Canadian,Caribbean,Chilean,Chinese,Contemporary,Continental-European,Cuban,Deli-Sandwiches,Dessert-Ice_Cream,Dim_Sum,Diner,Doughnuts,Dutch-Belgian,Eastern_European,Eclectic,Ethiopian,Family,Fast_Food,Filipino,Fine_Dining,French,Fusion,Game,German,Greek,Hawaiian,Hot_Dogs,Hungarian,Indian-Pakistani,Indigenous,Indonesian,International,Irish,Israeli,Italian,Jamaican,Japanese,Juice,Korean,Kosher,Latin_American,Lebanese,Malaysian,Mediterranean,Mexican,Middle_Eastern,Mongolian,Moroccan,North_African,Organic-Healthy,Pacific_Northwest,Pacific_Rim,Persian,Peruvian,Pizzeria,Polish,Polynesian,Portuguese,Regional,Romanian,Russian-Ukrainian,Scandinavian,Seafood,Soup,Southeast_Asian,Southern,Southwestern,Spanish,Steaks,Sushi,Swiss,Tapas,Tea_House,Tex-Mex,Thai,Tibetan,Tunisian,Turkish,Vegetarian,Vietnamese,smoker_false,smoker_true,drink_level_abstemious,drink_level_casual drinker,drink_level_social drinker,interest_eco-friendly,interest_none,interest_retro,interest_technology,interest_variety,personality_conformist,personality_hard-worker,personality_hunter-ostentatious,personality_thrifty-protector,religion_Catholic,religion_Christian,religion_Jewish,religion_Mormon,religion_none,budget_?,budget_high,budget_low,budget_medium,dress_preference_?,dress_preference_elegant,dress_preference_formal,dress_preference_informal,dress_preference_no preference,marital_status_?,marital_status_married,marital_status_single,marital_status_widow,ambience_?,ambience_family,ambience_friends,ambience_solitary,activity_?,activity_professional,activity_student,activity_unemployed,activity_working-class,placeID,rating,alcohol_Full_Bar,alcohol_No_Alcohol_Served,alcohol_Wine-Beer,price_high,price_low,price_medium,dress_code_casual,dress_code_formal,dress_code_informal,smoking_area_none,smoking_area_not permitted,smoking_area_only at bar,smoking_area_permitted,smoking_area_section,"accessibility_completely","accessibility_no_accessibility","accessibility_partially","Rambience_familiar","Rambience_quiet","area_closed","area_open"]
#classes = ["Dislike", "like", "Love"]

#dot_data = tree.export_graphviz(id3, out_file=None, feature_names=features, class_names=classes)
#graph = graphviz.Source(dot_data)
#graph.render("data1ID3")

#dot_data = tree.export_graphviz(cast, out_file=None, feature_names=features, class_names=classes)
#graph = graphviz.Source(dot_data)
#graph.render("data1Cast")
