from surprise import Dataset
from surprise import Reader
from surprise import SVD, NMF, KNNBasic
from surprise import evaluate, print_perf
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

train_df = pd.read_csv('aggregated_data/train_with_rating.csv')
train_df.drop(['Unnamed: 0'], axis=1)
reader = Reader(rating_scale=(0,2))

train_data = Dataset.load_from_df(train_df[['userID','placeID','rating']], reader)

train_data.split(n_folds=3)

print("\nSVD")
algo = SVD()
perf = evaluate(algo, train_data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nPMF")
# PMF
algo = SVD(biased=False)
perf = evaluate(algo, train_data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nNMF")
# NMF
algo = NMF()
perf = evaluate(algo, train_data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nUser Collaberative Filtering")
# User Collaberative Filtering
algo = KNNBasic(sim_options = {'user_based': True })
perf = evaluate(algo, train_data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)

print("\nItem Collaberative Filtering")
# Item Collaberative Filtering
algo = KNNBasic(sim_options = {'user_based': False })
perf = evaluate(algo, train_data, measures=['RMSE', 'MAE'], verbose=0)
print_perf(perf)
