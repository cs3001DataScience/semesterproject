# try chagning to entropy, try changing depth
# try different feature selections?
from sklearn.metrics import accuracy_score, mean_squared_error, mean_absolute_error
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import pandas as pd

input_df = pd.read_csv('./aggregated_data/total_train.csv')
Y = input_df['rating']
X = input_df.drop(columns=['rating','asdf','userID','placeID'])
feature_names = list(X.columns.values)

clf = RandomForestClassifier(criterion='entropy', n_estimators=100, max_depth=10,
                              random_state=0)
clf.fit(X, Y)

#try limiting to import features?
importances = clf.feature_importances_
print(importances)
#should be 2
print(clf.predict([[0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,1,0,0,0,1,1,0,0,0,0,1,0,0,1,0,0,1]]))

# -------------TESTING---------------
#get testing data
test_df = pd.read_csv('./aggregated_data/total_test.csv')
actual_rate = test_df['rating']
testing = test_df.drop(columns=['rating','asdf','userID','placeID'])

predictions = clf.predict(testing)
print(accuracy_score(actual_rate, predictions)) # 1 is best
print(mean_squared_error(actual_rate, predictions)) # 0 is best
print(mean_absolute_error(actual_rate, predictions)) # 0 is best

# Feature Selection:
new_features = X
new_feat_names = list(feature_names)
print(len(new_feat_names))
remove_cols = []
count = 0

average_import = float(sum(importances))/float(len(importances))

for meas in importances:
    if (meas < average_import-(average_import/2.0)):
        remove_cols.append(new_feat_names[count])
    count = count + 1
new_features = new_features.drop(columns=remove_cols)
for col in remove_cols:
    new_feat_names.remove(col)
print(new_features)
print(len(new_feat_names))

# Train and test on new feature set
# Y is the same
X = new_features
feature_names = new_feat_names

clf = RandomForestClassifier(criterion='entropy', n_estimators=100, max_depth=10,
                              random_state=0)
clf.fit(X, Y)

#try limiting to import features?
importances = clf.feature_importances_
print(importances)

# -------------TESTING---------------
# update testing data to remove features
#get testing data
test_df_new = test_df
actual_rate = test_df['rating']
testing_new = testing
testing_new = testing_new.drop(columns=remove_cols)

predictions_new = clf.predict(testing_new)
print(accuracy_score(actual_rate, predictions_new)) # 1 is best
print(mean_squared_error(actual_rate, predictions_new)) # 0 is best
print(mean_absolute_error(actual_rate, predictions_new)) # 0 is best