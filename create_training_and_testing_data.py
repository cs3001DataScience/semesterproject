import pandas as pd
import numpy as np

userprofile_df = pd.read_csv('./data/userprofile.csv')
userpayment_df = pd.read_csv('./data/userpayment.csv')
usercuisine_df = pd.read_csv('./data/usercuisine.csv')

userprofile_df = userprofile_df.drop(['latitude', 'longitude', 'weight', 'height', 'birth_year', 'transport', 'hijos', 'color'], axis=1) 

# # one hot encodes the data
# one_hot_cuisine = pd.get_dummies(usercuisine_df['Rcuisine'])
# usercuisine_df = usercuisine_df.drop(['Rcuisine'], axis=1)
# # joins the one hot encoded data and then aggregates it
# usercuisine_df = usercuisine_df.join(one_hot_cuisine).groupby('userID').max()

# one hot encode the payment type
one_hot_payment = pd.get_dummies(userpayment_df['Upayment'])
userpayment_df = userpayment_df.drop(['Upayment'], axis=1)
userpayment_df = userpayment_df.join(one_hot_payment).groupby('userID').max()

# join the dataframes all together into one dataframe
user_df = pd.merge(pd.merge(userprofile_df, userpayment_df, on='userID'), usercuisine_df, on='userID')

for feature in ['smoker', 'drink_level', 'interest', 'personality', 'religion', 'budget', 'dress_preference', 'marital_status', 'ambience', 'activity']:
    one_hot_userprofile = pd.get_dummies(user_df[feature])
    one_hot_userprofile.rename(columns=lambda x: feature + '_' + x, inplace=True)
    user_df = user_df.drop([feature], axis=1)
    user_df = user_df.join(one_hot_userprofile).groupby('userID').max()



### Restaurant stuff

chefmozcusine_df = pd.read_csv('./data/chefmozcuisine.csv')
chefmozaccepts_df = pd.read_csv('./data/chefmozaccepts.csv')
geoplaces2_df = pd.read_csv('./data/geoplaces2.csv', encoding = "ISO-8859-1")

# one hot encodes the restaurant cuisine data
one_hot_rcuisine = pd.get_dummies(chefmozcusine_df['Rcuisine'])
restaurantcuisine_df = chefmozcusine_df.drop(['Rcuisine'], axis=1)
restaurantcuisine_df = restaurantcuisine_df.join(one_hot_rcuisine).groupby('placeID').max()

# one hot encode the payment type accepted
one_hot_rpayment = pd.get_dummies(chefmozaccepts_df['Rpayment'])
restaurantpayment_df = chefmozaccepts_df.drop(['Rpayment'], axis=1)
restaurantpayment_df = restaurantpayment_df.join(one_hot_rpayment).groupby('placeID').max()

geoplaces2_df = pd.merge(pd.merge(chefmozaccepts_df, restaurantpayment_df, on='placeID'), geoplaces2_df, on='placeID')

geoplaces2_df = geoplaces2_df.drop(['latitude', 'longitude', 'the_geom_meter', 'name', 'address', 'fax', 'zip', 'other_services','url', 'city', 'state', 'country', 'franchise','alcohol'], axis=1)

for feature in ['price', 'dress_code', 'smoking_area', 'accessibility', 'Rambience', 'area']:
    one_hot_geoplaces = pd.get_dummies(geoplaces2_df[feature])
    geoplaces2_df = geoplaces2_df.drop([feature], axis=1)
    one_hot_geoplaces.rename(columns=lambda x: feature + '_' + x, inplace=True)
    geoplaces2_df = geoplaces2_df.join(one_hot_geoplaces).groupby('placeID').max()


# Join the features together

train_df = pd.read_csv('./aggregated_data/train_with_rating.csv')
test_df = pd.read_csv('./aggregated_data/test_with_rating.csv')
# join all of the things
combined_train_df = pd.merge(pd.merge(train_df, user_df, on='userID'), geoplaces2_df, on='placeID')
combined_test_df = pd.merge(pd.merge(test_df, user_df, on='userID'), geoplaces2_df, on='placeID')
# drop what we don't need
combined_train_df = combined_train_df.drop(['Unnamed: 0', 'Rpayment', 'Rcuisine'], axis=1)
combined_test_df = combined_test_df.drop(['Unnamed: 0', 'Rpayment', 'Rcuisine'], axis=1)

combined_train_df.to_csv("./aggregated_data/train_1.csv")
combined_test_df.to_csv("./aggregated_data/test_1.csv")

user_train_df = pd.merge(train_df, user_df, on='userID')
user_test_df = pd.merge(test_df, user_df, on='userID')

user_train_df.to_csv("./aggregated_data/user_train.csv")
user_test_df.to_csv("./aggregated_data/user_test.csv")