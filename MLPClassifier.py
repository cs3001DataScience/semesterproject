import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, accuracy_score
from sklearn.neural_network import MLPClassifier

train_df = pd.read_csv('aggregated_data/train_1.csv')
y_train = train_df['rating']
train_df = train_df.drop(['rating','asdf', 'userID', 'placeID'], axis=1)
test_df = pd.read_csv('aggregated_data/test_1.csv')
y_test = test_df['rating']
test_df = test_df.drop(['rating','asdf', 'userID', 'placeID'], axis=1)

clf = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(50, 40, 20), random_state=1)

clf.fit(train_df, y_train)

predictions = clf.predict(test_df)

print("Multilayered Perceptron Classifier on test")
print("Accuracy: ", accuracy_score(y_test, predictions))
print("Mean Squared Error: ", mean_squared_error(y_test, predictions))
print("Mean Absolute Error: ", mean_absolute_error(y_test, predictions))